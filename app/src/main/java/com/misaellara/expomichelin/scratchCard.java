package com.misaellara.expomichelin;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import dev.skymansandy.scratchcardlayout.listener.ScratchListener;
import dev.skymansandy.scratchcardlayout.ui.ScratchCardLayout;

public class scratchCard extends AppCompatActivity implements ScratchListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Get view reference
        ScratchCardLayout scratchCardLayout = findViewById(R.id.scratchCard);

        //Set the drawable (programmatically)
       // scratchCardLayout.setScratchDrawable(getResources().getDrawable(R.drawable.regalo));

        //Set scratch brush width
      //  scratchCardLayout.setScratchWidth(30f);
       // scratchCardLayout.setScratchWidthDip(30f);
        //Reveal full layout when some percent of the view is scratched
//        scratchCardLayout.setRevealFullAtPercent(40);

        //Scratching enable/disable
        //  scratchCardLayout.setScratchEnabled(true);

        //Remove all scratch made till now
        // scratchCardLayout.resetScratch();

        //Reveal scratch card (Shows the layout underneath the scratch)
        //  scratchCardLayout.revealScratch();
          scratchCardLayout.setScratchListener(this);

        //You'll have three main callback methods as scratch listeners
        //Scratch started

    }

    @Override
    public void onScratchComplete() {

    }

    @Override
    public void onScratchProgress(ScratchCardLayout scratchCardLayout, int i) {

    }

    @Override
    public void onScratchStarted() {

    }
}